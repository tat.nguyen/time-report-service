import os
from os import path
import pathlib
import glob
import json
from functools import lru_cache
from datetime import datetime
from contextlib import contextmanager
import pymongo
from pymongo import MongoClient
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from social_network_info import constant_information
import string
now = datetime.now()
class time_report:
    def __init__(self, social_network_type, field, sheet_idx):
        self.social_network_type = social_network_type
        self.field = field
        self.file_name = self.social_network_type + "_" + str(now.day) + "-" + str(now.month) + "-" + str(now.year) + ".txt"
        self.dir_data = str(pathlib.Path(__file__).parent.absolute()) + "\\data\\"
        self.path_file = "{}{}".format(self.dir_data, self.file_name)
        self.dir_google_cred = str(pathlib.Path(__file__).parent.absolute()) + "\\google_credentials\\"
        self.sheet_idx = sheet_idx

    @contextmanager
    def connect_database(self, url):
        connection = None
        try:
            connection = MongoClient(url, unicode_decode_error_handler='ignore')
            yield "success", connection
        except Exception:
            yield "fail", connection
        connection.close()

    def create_file(self):
        f = open(self.path_file, "w")
        f.close()
    
    def remove_file(self):
        os.chdir(self.dir_data)
        list_text_file = glob.glob("*.txt")
        if list_text_file:
            for file_ in list_text_file:
                social_type = file_.split("_")
                if social_type[0] == self.social_network_type:
                    os.remove(file_)
        
    def check_file_exist(self):
        os.chdir(self.dir_data)
        list_text_file = glob.glob("*.txt")
        if len(list_text_file) == 0:
            return False
        else:
            for file_ in list_text_file:
                if file_ == self.file_name:
                    return True
        return False
            
    def save_array_to_file(self, list_data):
        with open(self.path_file, "w", encoding='utf-8') as f:
            for data in list_data:
                line = str(data)
                f.write(line + "\n")

    def load_file_to_array(self):
        list_kol = []
        with open(self.path_file, "r", encoding='utf-8') as f:
            list_data = f.readlines()
        for data in list_data:
            try:
                list_kol.append(eval(data))
            except NameError:
                continue
        return list_kol
    def check_all_qualify(self, list_data):
        for data in list_data:
            if "qualify" not in data:
                print(data, "khong co fielqualify")
                return False
        return True
    def get_list_kol_qualify(self):
        count = 0
        info = constant_information
        # print(info)
        url = info[self.social_network_type]["url_database"]
        qualify = info[self.social_network_type]["qualify"]
        reaction_field = info[self.social_network_type]["average_reaction_field"]
        social_network_country = [ct_code for ct_code in qualify]
        print(self.check_file_exist())
        
        if self.check_file_exist() is False:
            self.remove_file()
            self.create_file()
            with self.connect_database(url) as response:
                status, connection = response
                if status == "success":
                    kols = connection[self.social_network_type]["kols"]
                    datakols = list(kols.find({"country_code": {"$in" : social_network_country}}))
            self.save_array_to_file(datakols)
        list_data = self.load_file_to_array()
        # c = 0
        # for idx, _ in enumerate(list_data):
        #     data = list_data[idx]
        #     if "qualify" not in data:
        #         c += 1
        #         print(data)
        #         break
        # return
        list_userid = []
        if not self.check_all_qualify(list_data):
            
            for idx, _ in enumerate(list_data):
                data = list_data[idx]
                if "user_id" in data and "country_code" in data:
                    print(data["user_id"])
                    user_id = data["user_id"]
                    if user_id == -1:
                        list_data[idx].update({"qualify":"NO"})
                    else:
                        list_userid.append(user_id)
            print("len_list_data", len(list_userid))
            # for i in list_userid:
            #     if not isinstance(i, int):
            #         print(i)
            #         return
            # return
            # print(list_userid, self.social_network_type)
            with self.connect_database(url) as response:
                status, connection = response
                if status == "success":
                    users = connection[self.social_network_type]["users"]
                    datausers = list(users.find({"_id" :{"$in" : list_userid}}))
                    # return
            datausers = {x["_id"]: x for x in datausers}
            print( "len datausers: ", len(datausers))
        for idx, _ in enumerate(list_data):
            data = list_data[idx]
            if "qualify" in data:
                continue
            if "user_id" in data and "country_code" in data:
                user_id = data["user_id"]
                country = data["country_code"][0]
                if user_id != -1 and user_id in datausers:
                    # print(datausers[user_id])
                    print("da check qualify")
                    count += 1
                    print(count)
                    datauser = datausers[user_id]
                    if "num_follower" in datauser and "interaction" in datauser:
                        average_reaction = datauser["interaction"][reaction_field]
                        num_follower = datauser["num_follower"]
                        try:
                            qualify_country = qualify[country]
                            print(country, user_id, num_follower, average_reaction)
                        except KeyError:
                            continue
                        if num_follower >= qualify_country["num_follower"]:
                            if average_reaction >= qualify_country[reaction_field]:
                                list_data[idx].update({"qualify": "YES"})
                                self.save_array_to_file(list_data)
                            else:
                                list_data[idx].update({"qualify": "NO"})
                                self.save_array_to_file(list_data)
                        else:
                            list_data[idx].update({"qualify": "NO"})
                            self.save_array_to_file(list_data)
                    else:
                        list_data[idx].update({"qualify": "NO"})
                        self.save_array_to_file(list_data)
        
        list_temp = []
        for idx, data in enumerate(list_data):
            if "qualify" in data and data["qualify"] == "YES":
                print(data["user_id"], data["qualify"])
                list_temp.append(data)
        print(len(list_data))
        return sorted(list_temp , key=lambda x: x["country_code"][0])[::-1]
    def format_googlesheet(self, sheet, rows):
        lascii = string.ascii_lowercase
        print("A1:{}{}".format(lascii[len(rows[0])-1].upper(), len(rows)))
        # return
        sheet.format("A1:{}{}".format(lascii[len(rows[0]) - 1].upper(), len(rows)), {"borders": 
        {"top": {
            "style": "SOLID",
            "width": 1,
        },
        "right": {
            "style": "SOLID",
            "width": 1,
        },
        "left": {
            "style": "SOLID",
            "width": 1,
        },
       "bottom": {
            "style": "SOLID",
            "width": 1,
        }
        }})
        sheet.format("A1:{}{}".format(lascii[len(rows[0]) - 1].upper(), len(rows)), {  "horizontalAlignment": "CENTER",
                                                    "textFormat": {"fontSize": 12}})
        sheet.format("A1:{}1".format(lascii[len(rows[0]) - 1].upper()), { "textFormat": {"bold": True}})

            
    def write_to_google_sheet(self, list_report):
        scope = ["https://spreadsheets.google.com/feeds",'https://www.googleapis.com/auth/spreadsheets',"https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]
        creds = ServiceAccountCredentials.from_json_keyfile_name(self.dir_google_cred + "Time_Report.json", scope)
        info = constant_information
        qualify = info[self.social_network_type]["qualify"]
        social_network_country = [ct_code for ct_code in qualify]
        print(self.dir_google_cred + "Time_Report.json")
        # service = Create_Service(self.dir_google_cred + "Time_Report.json", "Time Report", "v4", ["https://www.googleapis.com/auth/spreadsheets"])
        # spreadsheets = service.spreadsheets()
        client = gspread.authorize(creds)
        sheet1 = client.open("Time_Report").get_worksheet(self.sheet_idx)  # Open the spreadhseet
        sheet2 = client.open("Time_Report").get_worksheet(self.sheet_idx + 1)
        sheet1.clear()
        # return
        print(dir(sheet1))
        # data = sheet.get_all_records()  # Get a list of all records
        rows = [["hiip_user_id", "user_id", "country", "type", "time_report", self.field]]
        # type_count = {"3 days":0, "7 days":0, "14 days":0, "21 days":0, "30 days":0, "more":0}
        county_type = {x : {"3 days":0, "7 days":0, "14 days":0, "21 days":0, "30 days":0, "more":0} for x in social_network_country}
        print(county_type)
        for data in list_report:
            try:
                user_id = data["user_id"]
                hiip_user_id = data["hiip_user_id"]
                country = data["country_code"][0]
            except KeyError:
                print("error")
                continue
            time_rport = str(now)
            # print(user_id)
            if self.field in data:
                try:
                    field_time = datetime.fromtimestamp(data[self.field])
                except TypeError:
                    continue
                distance = now - field_time
                type_ = ""
                if distance.days <= 3:
                    type_ = "3 days"
                elif 3 < distance.days <= 7:
                    type_ = "7 days"
                elif 7 < distance.days <= 14:
                    type_ = "14 days"
                elif 14 < distance.days <= 21:
                    type_ = "21 days"
                elif 21 < distance.days <= 30:
                    type_ = "30 days"
                elif distance.days > 30:
                    type_ = "more"
                county_type[country][type_] += 1
                print("this", county_type, country)
                # type_count[type_] += 1
                row = [hiip_user_id, user_id, country, type_, time_rport, str(field_time)]
                rows.append(row)
        sheet1.insert_rows(rows)
        self.format_googlesheet(sheet1, rows)
        rows = [["Time_check", "Market", "3 days", "7 days", "14 days", "21 days", "30 days", "more"]]
        type_ = rows[0][2:]
        # print(county_type)
        for ct in county_type:
            row = [str(now), ct.upper()]
            row.extend([county_type[ct][x] for x in type_])
            rows.append(row)
        sheet2.insert_rows(rows)
        self.format_googlesheet(sheet2, rows)
    def main(self):
        list_kols = self.get_list_kol_qualify()
        self.write_to_google_sheet(list_kols)

# facebook = time_report("facebook", "last_time_crawl_post", 0)
# facebook.main()
# facebook = time_report("facebook", "last_time_analyze_interaction", 2)
# facebook.main()


insta = time_report("instagram", "last_time_crawl_post", 4)
insta.main()
insta = time_report("instagram", "last_time_analyze_interaction", 6)
insta.main()
# break


        
            
                            



                



        




    
