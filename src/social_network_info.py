
constant_information = {
    "facebook": {
        "url_database": "mongodb://bigdatateam:superbighiipasia@52.12.211.26:27017/facebook",
        "average_reaction_field" : "average_reaction",
        "qualify" :{
                "vi":{
                    "num_follower" : 5000,
                    "average_reaction" : 100, 
                },
                "th":{
                    "num_follower" : 1000,
                    "average_reaction" : 100, 
                }
            }
        },
    "instagram": {
        "url_database" : "mongodb://bigdatateam:superbighiipasia@52.12.186.127:27017/instagram",
        "average_reaction_field" : "average_like",
        "qualify" :{
                "vi":{
                    "num_follower" : 5000,
                    "average_like" : 100, 
                },
                "id":{
                    "num_follower" : 2000,
                    "average_like" : 100, 
                },
                "th":{
                    "num_follower" : 1000,
                    "average_like" : 100, 
                }
            }
        },
    "youtube": {
        "url_database": "mongodb://bigdatateam:superbighiipasia@52.10.232.207:27017/youtube",
        "average_reaction_field" : "average_view",
        "qualify" :{
                "vi":{
                    "num_follower" : 5000,
                    "average_reaction" : 5000, 
                },
                "id":{
                    "num_follower" : 5000,
                    "average_reaction" : 5000, 
                },
                "th":{
                    "num_follower" : 1000,
                    "average_reaction" : 100, 
                }
            }
        } 
    }